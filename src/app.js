// import './scripts/loadImages';
// import './scripts/vendors/LazyImages';
import './styles/app.scss';

// Set Routes to load JS on specific body classes only
import Router from './scripts/util/Router';
import common from './scripts/routes/Common';

// Populate Router instance with DOM routes
const routes = new Router({
  // All pages / Only one page
  common,
});

// Load Events
$(document).ready(() => routes.loadEvents());
