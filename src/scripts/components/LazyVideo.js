export default {
  init() {
    let video_container = document.querySelector('figure .lazy-video');

    video_container.addEventListener('click', function(event){
        let video_src = this.dataset.src;
        this.classList.add('loading');
        this.innerHTML = '<iframe src="https://player.vimeo.com/video/'+ video_src +'?autoplay=1 frameborder="0" webkitAllowFullScree mozallowfullscreen allowFullScreen></iframe>';
    }, false);
  },
}
