export default {
  init() {
    //.et tabs = document.querySelector('.btn--tabs');
    let tabs = document.getElementsByClassName("btn--tabs");
    for (let tab of tabs) {
      tab.addEventListener('click', function(event){
        let tabId = this.dataset.tabid;
        openTab(event, tabId);
      }, false);
    }

    function openTab(evt, tabId) {

      evt.preventDefault();
      
      // Declare all variables
      var i, tabcontent, tablinks;

      // Get all elements with class="tabcontent" and hide them
      tabcontent = document.getElementsByClassName("tabs__content");
      for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
      }

      // Get all elements with class="tablinks" and remove the class "active"
      tablinks = document.getElementsByClassName("btn--tabs");
      for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
      }

      // Show the current tab, and add an "active" class to the button that opened the tab
      document.getElementById(tabId).style.display = "block";
      evt.currentTarget.className += " active";
    }

    //document.getElementById('t1').style('display', 'block');
    document.getElementById("defaultOpen").click();
 },
}
