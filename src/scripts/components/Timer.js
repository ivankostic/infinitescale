export default {
  printTime() {
    let currentTime = new Date();
    let currentMinutes = currentTime.getMinutes();
    let currentSeconds = currentTime.getSeconds();
    let remainingSeconds;
    let remainingMinutes;

    if (currentMinutes >= 0 && currentMinutes < 15) {
      remainingMinutes = 15 - (currentMinutes + 1);
      if (currentSeconds === 0) {
        remainingMinutes = 15 - currentMinutes;
      }
    } else if (currentMinutes >= 15 && currentMinutes < 30) {
      remainingMinutes = 30 - (currentMinutes + 1);
      if (currentSeconds === 0) {
        remainingMinutes = 30 - currentMinutes;
      }
    } else if (currentMinutes >= 30 && currentMinutes < 45) {
      remainingMinutes = 45 - (currentMinutes + 1);
      if (currentSeconds === 0) {
        remainingMinutes = 45 - currentMinutes;
      }
    } else if (currentMinutes >= 45) {
      remainingMinutes = 60 - (currentMinutes + 1);
      if (currentSeconds === 0) {
        remainingMinutes = 60 - currentMinutes;
      }
    }

    if (currentSeconds === 0) {
      remainingSeconds = currentSeconds; // solves 60 second error and prints 0
    } else if (currentSeconds <= 59) {
      remainingSeconds = 60 - currentSeconds;
    }

    remainingSeconds = (
      remainingSeconds < 10
      ? "0"
      : "") + remainingSeconds;
    remainingMinutes = (
      remainingMinutes < 10
      ? "0"
      : "") + remainingMinutes;

    let currentTimeString = "00:" + remainingMinutes + ":" + remainingSeconds;
    $(".timer-value").html(currentTimeString);
  },
}
