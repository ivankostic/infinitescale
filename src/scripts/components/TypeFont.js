export default {
  init() {
     const WebFont = require('webfontloader');

     WebFont.load({
      google: {
        families: ['Arvo:400,400i,700','Poppins:400,700&&display=swap'],
      },
    });
  },
}
