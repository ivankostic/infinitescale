// JS components
import TypeFont from '../components/TypeFont';
import Tabs from '../components/Tabs';
//import '../vendors/featherlight';

export default {
  init() { // scripts here run on the DOM load event
    TypeFont.init();
    Tabs.init();
  },
  finalize() { // scripts here fire after init() runs
  },
};
